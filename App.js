import React, { useState, useEffect } from 'react';
import {
    ActivityIndicator,
    Button,
    Image,
    Share,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,Platform,ScrollView,Dimensions,TextInput,Clipboard
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import uuid from 'uuid';
import * as firebase from 'firebase';
import storage from '@react-native-firebase/storage';
console.disableYellowBox = true;
const {width,height}=Dimensions.get('window');
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
// import Clipboard from '@react-native-clipboard/clipboard';
const imageWidth=width-60;
const imageHeight=200;
const url =
    'https://firebasestorage.googleapis.com/v0/b/blobtest-36ff6.appspot.com/o/Obsidian.jar?alt=media&token=93154b97-8bd9-46e3-a51f-67be47a4628a';

const firebaseConfig = {
    apiKey: 'AIzaSyDKL4Dvos2-9kR6AoUomKlu7Bx17VbxkoE',
    authDomain: 'vision2-f57d5.firebaseapp.com',
    databaseURL: 'https://vision2-f57d5-default-rtdb.firebaseio.com',
    storageBucket: 'vision2-f57d5',
    messagingSenderId: '522323936979',
};
const google={
    GOOGLE_CLOUD_VISION_API_KEY: 'AIzaSyBfFSrPyfqRach0VllSGcjvmLU8UpVvzb8'
}
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}else {
    firebase.app(); // if already initialized, use that one
}
// firebase.initializeApp(firebaseConfig);
export default function App() {
    const [image, setImage] = useState(null);
    const [uploading, setUploading] = useState(false);
    const [googleResponse, setGoogleResponse] = useState(null);
    const [name,setName]=useState(null);
    const [id,setId]=useState(null);
    const [copiedText, setCopiedText] = useState([]);
    useEffect(() => {
        getPermissionAsync()
    }, []);
    const getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
            const result = await Permissions.getAsync(Permissions.CAMERA);
            if (permission.status !== 'granted') {
                const newPermission = await Permissions.askAsync(
                    Permissions.CAMERA_ROLL,
                );
                if (newPermission.status === 'granted') {
                    //its granted.
                }
            }
            if (result.status !== 'granted') {
                const newResult = await Permissions.askAsync(Permissions.CAMERA);
                if (newResult.status === 'granted') {
                    //its granted.
                }
            }
        }

    };
    const pickImage = async () => {
        if (Constants.platform.android) {
            // setCalled(false)
            const { canAskAgain } = await Permissions.getAsync(Permissions.CAMERA);
            await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (!canAskAgain) {
                Alert.alert('please grant permission in setting and Try');
            }
        }
        setImage(null);
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            quality: 0.5, base64: true
        });

        try{
            setUploading(true);
            if (!result.cancelled) {
                let uploadUrl = await uploadImageAsync(result.uri);
                // console.log('uploadUrl',result)
                setImage(uploadUrl);
                submitToGoogle(uploadUrl)
            }

        }catch(e){
            alert('Upload failed, sorry :(');
        }finally {
            setUploading(false);
        }

    };
    const takeImage = async () => {
        if (Constants.platform.android) {
            const { canAskAgain } = await Permissions.getAsync(Permissions.CAMERA);
            await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (!canAskAgain) {
                Alert.alert('please grant permission in setting and Try');
            }
        }
        setImage(null);
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            quality: 0.5, base64: true
        });

        try{
            setUploading(true);
            if (!result.cancelled) {
                let uploadUrl = await uploadImageAsync(result.uri);
                // console.log('uploadUrl',result)
                setImage(uploadUrl);
                submitToGoogle(uploadUrl)
            }

        }catch(e){
            alert('Upload failed, sorry :(');
        }finally {
            setUploading(false);
        }

    };
    const save=()=>{
        let data={}
        data['name']=name;
        data['id']=id;
        alert(name +'s id is '+id)
    }
    const submitToGoogle = async (image) => {
        try {
            setUploading(true)
            let body = JSON.stringify({
                requests: [
                    {
                        features: [
                            { type: 'LABEL_DETECTION', maxResults: 10 },
                            // { type: 'LANDMARK_DETECTION', maxResults: 5 },
                            // { type: 'FACE_DETECTION', maxResults: 5 },
                            // { type: 'LOGO_DETECTION', maxResults: 5 },
                            { type: 'TEXT_DETECTION', maxResults: 5 },
                            { type: 'DOCUMENT_TEXT_DETECTION', maxResults: 5 },
                            // { type: 'SAFE_SEARCH_DETECTION', maxResults: 5 },
                            // { type: 'IMAGE_PROPERTIES', maxResults: 5 },
                            // { type: 'CROP_HINTS', maxResults: 5 },
                            // { type: 'WEB_DETECTION', maxResults: 5 }
                        ],
                        image: {
                            source: {
                                imageUri: image
                            }
                        }
                    }
                ]
            });
            let response = await fetch(
                'https://vision.googleapis.com/v1/images:annotate?key=' +
                google['GOOGLE_CLOUD_VISION_API_KEY'],
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: body
                }
            );
            let responseJson = await response.json();
            setGoogleResponse(responseJson)
            setUploading(false)
        } catch (error) {
            console.log(error);
        }
    };
    async function uploadImageAsync(uri) {
        try{
            const ref =await storage().ref('black').putFile(uri);
            let imageRef =await storage().ref('black').getDownloadURL();
            return imageRef
        }catch(e){
           console.log(e)
        }
    }
    const copyToClipboard = (val) => {
        Clipboard.setString(val);
    };

    const fetchCopiedText = async () => {
        const text = await Clipboard.getString();
        setCopiedText(prev=>[...prev,text]);
    };
    const fetchEmpty = () => {
        setCopiedText([]);
    };
    const submit = () => {
        alert(copiedText.join(' '))
    };
    const _maybeRenderImage = () => {
        if (!image) {
            return;
        }

        return (
            <>
            <View
                style={{
                    marginTop: 20,
                    width: imageWidth,
                    borderRadius: 3,
                    elevation: 2
                }}
            >
              <View
                  style={{
                      borderTopRightRadius: 3,
                      borderTopLeftRadius: 3,
                      shadowColor: 'rgba(0,0,0,1)',
                      shadowOpacity: 0.2,
                      shadowOffset: { width: 4, height: 4 },
                      shadowRadius: 10,
                      overflow: 'hidden'
                  }}
              >
                   <Image source={{ uri: image }} style={{ width: imageWidth, height: 180 }} />


              </View>
                {/*<Button*/}
                    {/*style={{ marginBottom: 10 }}*/}
                    {/*onPress={() =>submitToGoogle()}*/}
                    {/*title="Analyze the text"*/}
                {/*/>*/}
                {
                    googleResponse &&
                        <View style={{height:imageHeight,backgroundColor:'white',borderRadius:5,overflow:'hidden'}}>
                    <ScrollView>
                        {
                            googleResponse?.responses[0]?.textAnnotations.map((text,index)=>{
                                 if(index==0) return null
                                return(
                            <TouchableOpacity  onPress={()=>copyToClipboard(text.description)} >
                                <View style={{ borderWidth: 1, borderColor: 'grey',borderStyle: 'dotted', marginVertical: 5,marginHorizontal:10,borderRadius:5,padding:5 }}>
                                    <Text>{text.description}</Text>
                                </View>
                            </TouchableOpacity >)
                        })
                        }

                    </ScrollView>
                        </View>
                }



            </View>
            {
                googleResponse &&
                <View
                    style={{
                        marginTop: 20,
                        width: imageWidth,
                        borderRadius: 3,
                        elevation: 2
                    }}
                >
                    <View style={{height:130,backgroundColor:'white',borderRadius:5,overflow:'hidden'}}>

                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:1,marginRight:5}}>
                            <Button
                                style={{ marginBottom: 10 }}
                                onPress={() =>fetchCopiedText()}
                                title="Copy"
                            />
                        </View>
                        <View style={{flex:1,marginLeft:5}}>
                            <Button
                                style={{ marginBottom: 10 }}
                                onPress={() =>fetchEmpty()}
                                title="Empty"
                            />
                        </View>
                        <View style={{flex:1,marginLeft:5}}>
                            <Button
                                style={{ marginBottom: 10 }}
                                onPress={() =>submit()}
                                title="Submit"
                            />
                        </View>
                    </View>
                        <Text style={{paddingHorizontal:10}}>{copiedText.length>0 ?copiedText.join(' '):null}</Text>

                    </View>

                </View>
            }


            </>
        );
    };
    const _maybeRenderUploadingOverlay = () => {
        if (uploading) {
            return (
                <View
                    style={[
                        StyleSheet.absoluteFill,
                        {
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            alignItems: 'center',
                            justifyContent: 'center',
                        },
                    ]}>
                  <ActivityIndicator color="#fff" animating size="large" />
                </View>
            );
        }
    };
    return(

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'#bfefef33' }}>
            {image ? null : (
                <Text
                    style={{
                        fontSize: 20,
                        marginBottom: 20,
                        textAlign: 'center',
                        marginHorizontal: 15,
                    }}>
                  Google Vision
                </Text>
            )}

            <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:30}}>
                <View style={{flex:1,marginRight:5}}>
                    <Button
                        onPress={pickImage}
                        title="Upload"
                    />
                </View>
                <View style={{flex:1,marginLeft:5}}>
                    <Button
                        onPress={takeImage}
                        title="Take an image"
                    />
                </View>


            </View>

            {_maybeRenderImage()}
            {_maybeRenderUploadingOverlay()}

          <StatusBar barStyle="default" />

        </View>

    )
}
const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderColor:'black'
    },
});
